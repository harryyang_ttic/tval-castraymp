#ifndef HELPER_H_INCLUDED
#define HELPER_H_INCLUDED

#include "stdafx.h"

//load functions
inline std::vector<VectorXf > parseDisparityPlane(const char* planefile)
{
    std::vector<VectorXf > planes;
    std::ifstream input(planefile);
     if (!input.is_open()) {
        std::cout << "Open file error: " << planefile << std::endl;
        return planes;
    }
    string line;
    float tmp;
    char c;
    while(getline(input,line))
    {
        if(line[0]=='c')
            continue;
        istringstream is(line);
        VectorXf plane(5);
        is >> tmp;
        is >> plane(0);
        is >> plane(1);
        is >> plane(2);
        is >> plane(3);
        is >> tmp;
        is >> tmp;
        is >> tmp;
        is >> tmp;
		is >> c;
		if (c == 'Y')
			plane(4)=1;
		else
			plane(4)=0;
        planes.push_back(plane);

    }
    return planes;
}

inline Matrix3f ReadProjMatrix(const char* filename, double& fx, double& fy, double& ox, double& oy)
{
    Matrix3f M_Proj;
    if(!strcmp(filename,""))
    {
        M_Proj(0, 0) = 1019.305;
        M_Proj(0, 1) = 0.0;
        M_Proj(0, 2) = 789.380;
        M_Proj(1, 0) = 0.0;
        M_Proj(1, 1) = 1019.305;
        M_Proj(1, 2) = 612.212;
        M_Proj(2, 0) = 0.0;
        M_Proj(2, 1) = 0.0;
        M_Proj(2, 2) = 1.0;
    }
    else
    {
        ifstream v_c(filename);
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                v_c>>M_Proj(i,j);
            }
        }
    }
    fx=M_Proj(0,0);
    fy=M_Proj(1,1);
    ox=M_Proj(0,2);
    oy=M_Proj(1,2);
    return M_Proj;
}

inline Matrix4f ReadVCMatrix(const char* vc_name)
{
    Matrix4f M_v_c;
    std::ifstream v_c(vc_name);
    for(int i=0;i<4;i++)
    {
        for(int j=0;j<4;j++)
        {
            v_c>>M_v_c(i,j);
        }
    }
    v_c.close();
    return M_v_c;
}

inline double ReadBaselineRation(const char* baseline_ratio_file)
{
    double baseline_ratio;
    if(strcmp(baseline_ratio_file,""))
    {
        ifstream myfile(baseline_ratio_file);
        myfile>>baseline_ratio;
        myfile.close();
    }
    else baseline_ratio=1;
    return baseline_ratio;
}

inline std::vector<VectorXf> parsePlanefile(const char *planefile)
{
    std::vector<VectorXf> planes;
    std::ifstream input(planefile);
    if (!input.is_open()) {
        std::cout << "Open file error: " << planefile << std::endl;
        return planes;
    }
    string line;
    float tmp;
	char c;
    while(getline(input,line))
    {
        if(line[0]=='c')
            continue;
        istringstream is(line);
        VectorXf plane(5);
        is >> tmp;
        is >> tmp;
        is >> tmp;
        is >> tmp;
        is >> tmp;
        is >> plane(0);
        is >> plane(1);
        is >> plane(2);
        is >> plane(3);
		is >> c;
		if (c == 'Y')
			plane(4)=1;
		else
			plane(4)=0;
        planes.push_back(plane);
    }
    return planes;
}

inline std::vector<VectorXf> parseVelodynefile(const char *Velodynefile)
{
    std::vector<VectorXf> laser_points;
    std::ifstream input(Velodynefile);
    if (!input.is_open()) {
        std::cout << "Open file error: " << Velodynefile << std::endl;
        return laser_points;
    }

    float tmp;
    while (!input.eof()) {
        VectorXf point(6);
        input >> tmp;
        point(0) = tmp / 100.0;
        input >> tmp;
        point(1) = tmp / 100.0;
        input >> tmp;
        point(2) = tmp / 100.0;
        input >> tmp;
        point(3) = tmp;
        input >> tmp;
        point(4) = tmp;
        input >> tmp;
        point(5) = tmp;
        laser_points.push_back(point);
    }
    return laser_points;
}

inline MatrixXf loadSegfile(const char *segfile)
{

    rev::Image<unsigned short> segImage;
    segImage = rev::read16bitImageFile(segfile);

    MatrixXf segment(segImage.width(), segImage.height());
    for (int u = 0; u < segImage.width(); u++)
        for (int v = 0; v < segImage.height(); v++)
            segment(u, v) = segImage(u, v);

    return segment;

}

inline MatrixXf loadDisparityFile(const char *disparityfile)
{
    rev::Image<unsigned short> disparityImage;
    disparityImage = rev::read16bitImageFile(disparityfile);

    cout<<disparityImage.width()<<" "<<disparityImage.height()<<endl;

    MatrixXf disparity(disparityImage.height(), disparityImage.width());
    cout<<disparity.rows()<<" "<<disparity.cols()<<endl;
    for (int u = 0; u < disparityImage.height(); u++)
        for (int v = 0; v < disparityImage.width(); v++)
            disparity(u, v) = disparityImage(v, u);



    return disparity;
}


inline Matrix4f ReadInitialMatrix(const char* M_init_name)
{
    Matrix4f M_init;
    std::ifstream myfile(M_init_name);
    for(int i=0;i<4;i++)
    {
        for(int j=0;j<4;j++)
        {
            myfile>>M_init(i,j);
        }
    }
    myfile.close();
    return M_init;
}

//Save Functions
void SaveLaserPlaneDisparity(vector<VectorXf> laser_points_save, vector<VectorXf> planes_save, vector<VectorXf> disparity_planes_save, string point_name)
{
    string mylaserfiles="laser_points_"+string(point_name),
    myplanefiles="planes_"+string(point_name),
    mydisparityplanes="disparity_planes_"+string(point_name);
    std::ofstream mylaserfile(mylaserfiles.c_str()),myplanefile(myplanefiles.c_str()),mydisparityplane(mydisparityplanes.c_str());
    for(int i=0;i<laser_points_save.size();i++)
    {
        mylaserfile<<laser_points_save[i](0)<<" "<<laser_points_save[i](1)<<" "<<laser_points_save[i](2)<<std::endl;
        myplanefile<<planes_save[i](0)<<" "<<planes_save[i](1)<<" "<<planes_save[i](2)<<std::endl;
        mydisparityplane<<disparity_planes_save[i](0)<<" "<<disparity_planes_save[i](1)<<" "<<disparity_planes_save[i](2)<<" "<<disparity_planes_save[i](3)<<std::endl;
    }
    mylaserfile.close();
    myplanefile.close();
    mydisparityplane.close();
}

inline void SaveLaserPlaneClean(vector<Vector3f> laser_in_camera, vector<Vector3f> intersections, vector<VectorXf> laser_points_save, vector<VectorXf> planes_save, string point_name, double thresh=0.15)
{
    string mylaserfile="laser_points_clean_"+string(point_name),
    myplanefile="planes_clean_"+string(point_name);
    std::ofstream mylaserfile2(mylaserfile.c_str()),myplanefile2(myplanefile.c_str());
    for(int i=0;i<laser_points_save.size();i++)
    {
        double totaldist=(laser_in_camera[i][0]-intersections[i][0])*(laser_in_camera[i][0]-intersections[i][0])
        +(laser_in_camera[i][1]-intersections[i][1])*(laser_in_camera[i][1]-intersections[i][1])
        +(laser_in_camera[i][2]-intersections[i][2])*(laser_in_camera[i][2]-intersections[i][2]);
        totaldist=sqrt(totaldist);
        double totallength=laser_in_camera[i][0]*laser_in_camera[i][0]
        +laser_in_camera[i][1]*laser_in_camera[i][1]
        +laser_in_camera[i][2]*laser_in_camera[i][2];
        totallength=sqrt(totallength);
        double ratio=totaldist/totallength;
        if(ratio>thresh)
            continue;
        mylaserfile2<<laser_points_save[i][0]<<" "<<laser_points_save[i][1]<<" "<<laser_points_save[i][2]<<std::endl;
        myplanefile2<<planes_save[i][0]<<" "<<planes_save[i][1]<<" "<<planes_save[i][2]<<std::endl;
    }
    mylaserfile2.close();
    myplanefile2.close();
}

void SaveLaserInCameras(vector<Vector3f> laser_in_camera, string point_name)
{
    string outputfilename="lasercam_"+string(point_name);
    ofstream outputfile(outputfilename.c_str());
    for (unsigned i = 0; i < laser_in_camera.size(); i++) {
        outputfile << laser_in_camera[i](0) << " " << laser_in_camera[i](1) << " " << laser_in_camera[i](2) << std::endl;
    }
    outputfile.close();
}

void SaveIntersections(vector<Vector3f> intersections, string point_name)
{
    string outputfilename="raytrace_"+string(point_name);
    ofstream outputfile(outputfilename.c_str());
    for (unsigned i = 0; i < intersections.size(); i++)
    {
        outputfile << intersections[i](0) << " " << intersections[i](1) << " " << intersections[i](2) << std::endl;
    }
    outputfile.close();
}



#endif // HELPER_H_INCLUDED
