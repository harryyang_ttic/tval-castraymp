#ifndef STDAFX_H
#define STDAFX_H

#include <eigen3/Eigen/Dense>

#include <omp.h>
#include "dirent.h"
#include "string.h"
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <queue>
#include <fstream>
#include <iostream>
#include <revlib.h>
#include "cmdline.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"

using namespace Eigen;
using namespace std;

#endif
