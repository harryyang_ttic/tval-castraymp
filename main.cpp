#include"stdafx.h"
#include"Helper.h"
#include"ClosestSegment.h"

using namespace cv;

const double EPS=1e-6;


string velodyne_path, seg_path, seg_plane_path, calibration_file, res_path;
string baseline_ratio_file, project_matrix_file, initial_matrix_file;
double baseline, baseline_ratio;
int left_crop,right_crop,thresh;
double fx,fy,ox,oy;
double SCORE_PIXEL;

bool USE_LIDAR, DISCARD_NO_HIT;

double save_thresh;
const int soft_thresh=15;
const double max_point_dist=80;

inline VectorXf Initialize(int n, int* a=NULL)
{
    VectorXf v(n);
    for(int i=0;i<n;i++)
    {
        if(a!=NULL)
            v[i]=a[i];
        else
            v[i]=0;
    }
    return v;
}

void ParseArguments(int argc, char* argv[])
{
    cmdline::parser commandParser;
    commandParser.add<string>("baseline_ratio", 'r', "baseline_ratio_file", false, "");
    commandParser.add<string>("project_matrix", 'p', "project_matrix_file", false, "");
    commandParser.add<double>("baseline", 'b', "baseline value", false, 0.558253);
    commandParser.add<int>("left_crop", 'l', "left crop value", false, 150);
    commandParser.add<int>("right_crop", 's', "right crop value", false, 50);
    commandParser.add<int>("threshold",'t',"threshold value",false,10);
    commandParser.add<bool>("use_lidar",'u',"use ground truth lidar",false,true);
    commandParser.add<bool>("discard_no_hit",'d',"discard points that do not hit anything", false,true);
    commandParser.add<string>("initial_matrix",'i',"intial matrix file that decides which point to score", false, "");
    commandParser.add<double>("pixel_error",'e',"what is the number of error pixels to score?",false,3);
    commandParser.add<double>("save_clean_threshold",'c',"what is the inlier distance you want to save for optimization purpose?",false,0.15);
    commandParser.add("verbose", 'v', "verbose");
    commandParser.add("help", 'h', "display this message");
    commandParser.set_program_name("castray");
    commandParser.footer("velo_dir, seg_dir, seg_plane_dir, calib_matrix_file, res_dir");
    bool isCorrectCommandline = commandParser.parse(argc, argv);
	// Check arguments
	if (!isCorrectCommandline) {
		std::cerr << commandParser.error() << std::endl;
	}
	if (!isCorrectCommandline || commandParser.exist("help") || commandParser.rest().size() < 5) {
		std::cerr << commandParser.usage() << std::endl;
		exit(1);
	}

	velodyne_path=commandParser.rest()[0];
	seg_path=commandParser.rest()[1];
	seg_plane_path=commandParser.rest()[2];
	calibration_file=commandParser.rest()[3];
	res_path=commandParser.rest()[4];

    baseline_ratio_file=commandParser.get<string>("baseline_ratio");
    project_matrix_file=commandParser.get<string>("project_matrix");

    baseline=commandParser.get<double>("baseline");
    left_crop=commandParser.get<int>("left_crop");
    right_crop=commandParser.get<int>("right_crop");
    thresh=commandParser.get<int>("threshold");

    USE_LIDAR=commandParser.get<bool>("use_lidar");
    DISCARD_NO_HIT=commandParser.get<bool>("discard_no_hit");

    SCORE_PIXEL=commandParser.get<double>("pixel_error");
    save_thresh=commandParser.get<double>("save_clean_threshold");

    initial_matrix_file=commandParser.get<string>("initial_matrix");
    if(initial_matrix_file=="")
    {
        initial_matrix_file=calibration_file;
    }
}

Vector3f GetOrigin(Matrix4f M_v_c)
{
    VectorXf origin;
    int a[4]={0,0,0,1};
    origin=Initialize(4,a);
    origin = M_v_c * origin;
    Vector3f ray_ori;
    ray_ori(0) = origin(0);
    ray_ori(1) = origin(1);
    ray_ori(2) = origin(2);
    std::cout << "ray origin: " << ray_ori(0) << " " << ray_ori(1) << " " << ray_ori(2) << std::endl;
    return ray_ori;
}

ostream& operator<<(ostream& os, const Vector3f vec)
{
    os<<vec(0)<<" "<<vec(1)<<" "<<vec(2);
    return os;
}

//compare functions
inline void ComparePoints(std::vector<Vector3f > laser_in_camera, std::vector<Vector3f > intersections, std::vector<VectorXf > disparity_planes, vector<VectorXf> laser_points, double& threepixelaccuracy, double& threepixelaccuracy2, MatrixXf M_v_c, int height=848, int width=1600)
{
    int num=laser_in_camera.size();
    int fiveperc=0,tenperc=0,totalpts=0,threepixel=0, threepixel2=0;

    for(int i=0;i<num;i++)
    {
        if(!(laser_in_camera[i][0]==0 && laser_in_camera[i][1]==0 && laser_in_camera[i][2]==0))
        {
            totalpts++;
            //cout<<"total:"<<totalpts<<endl;
            double dist=(laser_in_camera[i][0]-intersections[i][0])*(laser_in_camera[i][0]-intersections[i][0])
            +(laser_in_camera[i][1]-intersections[i][1])*(laser_in_camera[i][1]-intersections[i][1])
            +(laser_in_camera[i][2]-intersections[i][2])*(laser_in_camera[i][2]-intersections[i][2]);
            dist=sqrt(dist);
            double length=laser_in_camera[i][0]*laser_in_camera[i][0]
            +laser_in_camera[i][1]*laser_in_camera[i][1]
            +(laser_in_camera[i][2])*(laser_in_camera[i][2]);
            length=sqrt(length);
           // cout<<length<<endl;
            double ratio=dist/length;
            if(ratio<0.05)
                fiveperc++;
            if(ratio<0.1)
                tenperc++;

            double X=laser_in_camera[i][0], Y=laser_in_camera[i][1], Z=laser_in_camera[i][2];
            double M1=disparity_planes[i][0], M2=disparity_planes[i][1], M3=disparity_planes[i][3];
           // cout<<M1<<" "<<M2<<" "<<M3<<endl;
           // cout<<X<<" "<<Y<<" "<<Z<<endl;
            double d_predict=fx*baseline/(baseline_ratio*Z);
            double e_predict=M1*(fx*X/Z+ox)+M2*(fy*Y/Z+oy)+M3;
            if(fabs(d_predict-e_predict)<=3)
            {
                threepixel++;
            }

            Vector4f intersection_in_velodyne;
            intersection_in_velodyne(0)=intersections[i][0];
            intersection_in_velodyne(1)=intersections[i][1];
            intersection_in_velodyne(2)=intersections[i][2];
            intersection_in_velodyne(3)=1;
            intersection_in_velodyne=M_v_c.inverse()*intersection_in_velodyne;
            double dist2=fabs(intersection_in_velodyne(0)-laser_points[i][0]);
            if(dist2/(laser_points[i][0]*laser_points[i][0])<SCORE_PIXEL/(fx*baseline))
                threepixel2++;
        }
    }
    threepixelaccuracy=(double)threepixel/totalpts;
    threepixelaccuracy2=(double)threepixel2/totalpts;
}

void ReadData(vector<vector<VectorXf> >& laser_points_all, vector<MatrixXf>& segments_all, vector<vector<VectorXf> >& planes_all, vector<vector<VectorXf> >& bb_all, vector<vector<VectorXf> >& disparity_planes_all, vector<string>& point_name, int& m, int& n)
{
    DIR* VelodyneDir = opendir(velodyne_path.c_str());
    if (!VelodyneDir)
    {
        cout<<"opening velodyne folder error"<<endl;
        exit(-1);
    }

    laser_points_all.clear();
    segments_all.clear();
    planes_all.clear();
    disparity_planes_all.clear();
    bb_all.clear();
    point_name.clear();

    m=0;
    n=0;

    int velId=0;
    struct dirent* vFile;
    while ((vFile = readdir(VelodyneDir)) != NULL)
    {
        if (strcmp(vFile->d_name, ".") == 0) continue;
        if (strcmp(vFile->d_name, "..") == 0) continue;
        velId++;

        cout << "Opening laser file: " << vFile->d_name << std::endl;
        string fullpath=velodyne_path+string(vFile->d_name);
        vector<VectorXf> laser_point = parseVelodynefile(fullpath.c_str());
        laser_points_all.push_back(laser_point);
        cout << "parse velodyne file finished." << std::endl;

        fullpath=seg_path+string(vFile->d_name);
        fullpath=fullpath.substr(0,fullpath.length()-10);
        string segpath=fullpath+"_seg.png";
        MatrixXf segment = loadSegfile(segpath.c_str());
        segments_all.push_back(segment);
        cout << "load seg file finished." << std::endl;

        fullpath=seg_plane_path+string(vFile->d_name);
        fullpath=fullpath.substr(0,fullpath.length()-10);
        string seg_plane_path=fullpath+"_seg_planes.txt";
        vector<VectorXf> planes = parsePlanefile(seg_plane_path.c_str());
        vector<VectorXf> disparityPlanes=parseDisparityPlane(seg_plane_path.c_str());
        //fix 0929
        for(int i=0;i<planes.size();i++)
        {
            double a=disparityPlanes[i](0), b=disparityPlanes[i](1), c=disparityPlanes[i](3);
            planes[i](0)=fx*a/(fx*baseline),
            planes[i](1)=fy*b/(fx*baseline),
            planes[i](2)=(a*ox+b*oy+c)/(fx*baseline);
        }
        //end fix
        planes_all.push_back(planes);
        disparity_planes_all.push_back(disparityPlanes);
        cout << "parse plane file finished." << std::endl;

        if (m==0 && n==0){
            m = segment.rows();
            n = segment.cols();
        }
        else
        {
            if (m!=segment.rows() || n!=segment.cols())
            {
                std::cout << "error: image size does not match!" << std::endl;
            }
        }
        vector<VectorXf> bb = computeBoundingBox(segment);
        bb_all.push_back(bb);

        point_name.push_back(string(vFile->d_name));
    }
    closedir(VelodyneDir);
}

bool IsOnSameSide(Vector3f Pt, VectorXf plane)
{
    Vector3f norm;
    norm(0)=plane(0);
    norm(1)=plane(1);
    norm(2)=plane(2);
    Vector3f ori(0,0,0);
    Vector3f ptOnPlane(0,0,0);
    if(plane(0)!=0)
    {
        ptOnPlane(0)=1/plane(0);
    }
    else if(plane(1)!=0)
    {
        ptOnPlane(1)=1/plane(1);
    }
    else if(plane(2)!=0)
        ptOnPlane(2)=1/plane(2);
    else
        return false;
    double dot1=norm.dot(Pt-ptOnPlane), dot2=norm.dot(ori-ptOnPlane);
    if(dot1>=0 && dot2>=0)
        return true;
    if(dot1<=0 && dot2<=0)
        return true;
    return false;
}

int ProcessOccludedPoint(Vector3f point_in_camera, Vector3f ray_ori, Matrix3f M_proj, MatrixXf segment, vector<VectorXf> planes, int width, int height, Vector3f real_intersection, std::vector<VectorXf> bb, Vector3f& intersection, int& dist)
{
    Vector3f U_in=M_proj*point_in_camera, O_in=M_proj*ray_ori;

    double X_in=U_in(0)/U_in(2), Y_in=U_in(1)/U_in(2);

    double XO_in=O_in(0)/O_in(2),YO_in=O_in(1)/O_in(2);
    Vector2f U_in2d(X_in,Y_in), O_in2d(XO_in,YO_in);

    Vector3f ray = point_in_camera-ray_ori;
    Vector3f ray_unit=ray/ray.norm();

    Vector2f ray_2d= U_in2d-O_in2d;
    Vector2f image_unit=ray_2d/ray_2d.norm();

    double ratio=ray.norm()/ray_2d.norm();
    int n=0;
    int lastPlaneId=-1;
    bool flag=false;
    while(true)
    {
        n++;
        Vector2f pos_2d=O_in2d+n*image_unit;
        Vector3f pos_3d=ray_ori+n*ray_unit*ratio;
        int image_x=pos_2d(0), image_y=pos_2d(1);
       // cout<<image_x<<" "<<image_y<<endl;
        if(image_x<0 || image_x>=width || image_y<0 || image_y>=height)
            break;
        int planeId=segment(image_x,image_y);
        //cout<<planeId<<endl;
        if(planeId==lastPlaneId)
            continue;
        VectorXf plane=planes[planeId];
        bool relativePos=IsOnSameSide(pos_3d,plane);
       // cout<<relativePos<<endl;
        if(relativePos==false)
        {
            flag=true;
            break;
        }
        lastPlaneId=planeId;
    }
    if(lastPlaneId!=-1)
    {
        if(real_intersection==Vector3f(0,0,0) || flag==true)
        {
            VectorXf plane=planes[lastPlaneId];
            Vector3f plane_coef;
            plane_coef(0) = plane(0)*baseline_ratio;
            plane_coef(1) = plane(1)*baseline_ratio;
            plane_coef(2) = plane(2)*baseline_ratio;
            double coef = plane_coef.transpose() * ray_unit;
            if(abs(coef)<=EPS)
                return -1;
            double s = (plane(3) + plane_coef.transpose() * ray_ori) / -coef;
            intersection = ray_ori + s * ray_unit;
            //project_back
            Vector3f i_in=M_proj*intersection;
            int xi_in=i_in(0)/i_in(2), yi_in=i_in(1)/i_in(2);
            Vector2i vi_in(xi_in, yi_in);
            dist=findNearestSegment_approx(vi_in,bb,lastPlaneId);
        }
    }
    return lastPlaneId;
}

//#define OPTIMIZE

int main(int argc, char *argv[])
{
    ParseArguments(argc, argv);
	omp_set_num_threads(omp_get_max_threads());
    Matrix4f M_v_c=ReadVCMatrix(calibration_file.c_str());
    baseline_ratio=ReadBaselineRation(baseline_ratio_file.c_str());
    Matrix3f M_Proj=ReadProjMatrix(project_matrix_file.c_str(), fx, fy, ox, oy);
    Vector3f ray_ori=GetOrigin(M_v_c);
    Matrix4f M_init=ReadInitialMatrix(initial_matrix_file.c_str());

    vector<vector<VectorXf> > laser_points_all;
    vector<MatrixXf> segments_all;
    vector<vector<VectorXf> > planes_all;
    vector<vector<VectorXf> > bb_all;
    vector<vector<VectorXf> > disparity_planes_all;
    vector<string> point_name;
    int m=0, n=0;

    ReadData(laser_points_all,segments_all,planes_all,bb_all,disparity_planes_all,point_name, m,n);

    unsigned int total_points = 0;
    for (unsigned i=0; i<laser_points_all.size(); i++)
        total_points += laser_points_all[i].size();


#ifdef OPTIMIZE
    ofstream myfile("purtube.txt");
    double delta_x, delta_y, delta_z, delta_base;
    for(delta_x=-0.01;delta_x<=0.011;delta_x+=0.01)
    {
        for(delta_y=-0.01;delta_y<=0.011;delta_y+=0.01)
        {
            for(delta_z=-0.01;delta_z<=0.011;delta_z+=0.01)
            {
              //  for(delta_base=-0.01;delta_base<=0.011;delta_base+=0.01)
               // {
                    M_v_c(0,3)=0.1103774359+delta_x;
                    M_v_c(1,3)=-0.0399891386+delta_y;
                    M_v_c(2,3)=-0.9868361475+delta_z;
                 //   baseline_ratio=0.9712869744+delta_base;
                    ray_ori(0)=0.1103774359+delta_x;
                    ray_ori(1)=-0.0399891386 +delta_y;
                    ray_ori(2)=-0.9868361475+delta_z;
#endif

    vector<double> threepixels;

    string resultFileName=res_path+"/result.txt";
    ofstream result_file(resultFileName.c_str());
    #pragma omp parallel for
    for (unsigned vel=0; vel<laser_points_all.size(); vel++)
    {
        int ptnum=laser_points_all[vel].size();

        vector<Vector3f> laser_in_camera_single(ptnum);
        vector<Vector3f> intersections_single(ptnum);
        vector<VectorXf> disparity_plane_single(ptnum);
        vector<VectorXf> planes_single(ptnum);
        vector<VectorXf> laser_points_single(ptnum);

        for (int p = 0; p < ptnum; p++)
        {
            if (p % 10000 == 0) std::cout << "." << std::flush;
            VectorXf disparity_plane=Initialize(4);
            VectorXf h_plane=Initialize(3);
            Vector3f intersection(0,0,0);
            VectorXf point = laser_points_all[vel][p];

            Vector3f soft_intersection(0,0,0);
            VectorXf soft_h_plane=Initialize(3);
            VectorXf soft_disparity_plane=Initialize(4);

            if (point(0) == 0 && point(1) == 0 && point(2) == 0)
            {
                laser_in_camera_single[p]=intersection;
                intersections_single[p]=intersection;
                disparity_plane_single[p]=Initialize(4);
                planes_single[p]=Initialize(3);
                laser_points_single[p]=Initialize(3);
                continue;
            }

            VectorXf X(4);
            X(0) = point(0);
            X(1) = point(1);
            X(2) = point(2);
            X(3) = 1;
            VectorXf X1 = M_v_c * X, X2=M_init * X;
            Vector3f X_trans, X_trans2;
            X_trans(0)=X1(0);
            X_trans(1)=X1(1);
            X_trans(2)=X1(2);
            X_trans2(0)=X2(0);
            X_trans2(1)=X2(1);
            X_trans2(2)=X2(2);

            Vector3f UV2=M_Proj*X_trans2;
            float u2=UV2(0)/UV2(2), v2=UV2(1)/UV2(2);

            if(u2-1<=left_crop || u2>=m-right_crop || v2-1<=0 || v2>=n-1 || X2(2)<=0)
            {
                laser_in_camera_single[p]=intersection;
                intersections_single[p]=intersection;
                disparity_plane_single[p]=Initialize(4);
                planes_single[p]=Initialize(3);
                laser_points_single[p]=Initialize(3);
                continue;
            }

            // Discard the laser point if it is projected outside the camera frame
            laser_in_camera_single[p] = X_trans;
            laser_points_single[p]=point;
            Vector3f ray_unit = X_trans - ray_ori;
            ray_unit=ray_unit/ray_unit.norm();

            float mindist = FLT_MAX, softMinDist=FLT_MAX;

            for (int i = 0; i < planes_all[vel].size(); i++)
            {
                double u=-1, v=-1, s=-1;

                VectorXf plane = planes_all[vel][i];

                if (plane[4] == 0) continue; // The plane is marked as in-valid
                Vector3f plane_coef;
                plane_coef(0) = plane(0)*baseline_ratio;
                plane_coef(1) = plane(1)*baseline_ratio;
                plane_coef(2) = plane(2)*baseline_ratio;
                double coef = plane_coef.transpose() * ray_unit;
                VectorXf X_in;
                if(abs(coef)<=EPS)
                    continue;

                s = (plane(3) + plane_coef.transpose() * ray_ori) / -coef;
                X_in = ray_ori + s * ray_unit;
                Vector3f U_in = M_Proj * X_in;
                u = U_in(0) / U_in(2);
                v = U_in(1) / U_in(2);

                if (s < 0)
                    continue;

                if (u - 1 <= 0 || u >= m-1)
                    continue;
                if (v - 1 <= 0 || v >= n-1)
                    continue;

                double ss;

                if(USE_LIDAR)
                    ss=(X1(0)-X_in(0))*(X1(0)-X_in(0))+(X1(1)-X_in(1))*(X1(1)-X_in(1))+(X1(2)-X_in(2))*(X1(2)-X_in(2));
                else
                    ss=s;

                if (segments_all[vel](round(u), round(v)) == i) // Case 1: Intersection point fall into the same segment region
                {
                    if (ss < mindist && s > 0)
                    {
                        mindist = ss;
                        intersection = ray_ori + s * ray_unit;
                        disparity_plane=disparity_planes_all[vel][i];
                        h_plane=plane;
                    }
                }
                else
                {
                    Vector2i uv;
                    uv(0) = round(u);
                    uv(1) = round(v);
                    int dist = findNearestSegment_approx(uv, bb_all[vel], i);
                    // if intersections are too faraway, and are not occluded, we use soft threshold
                    if (dist <= soft_thresh && ss<softMinDist && s>0)
                    {
                        softMinDist=ss;
                        soft_intersection=ray_ori+s*ray_unit;
                        soft_disparity_plane=disparity_planes_all[vel][i];
                        soft_h_plane=plane;
                    }

                    if (dist > thresh)
                        continue;
                    if(ss<mindist && s>0)
                    {
                        mindist = ss;
                        intersection = ray_ori + s * ray_unit;
                        disparity_plane=disparity_planes_all[vel][i];
                        h_plane=plane;
                    }
                 }
            } // end of loop for planes

            //occluded points
            //int OccludedPlaneId=ProcessOccludedPoint(laser_in_camera_single[p],ray_ori,M_Proj,segments_all[vel],planes_all[vel],m,n,intersection,bb_all[vel],new_intersection,dist);
            /*if(intersection!=Vector3f(0,0,0) && dist<soft_thresh && fabs(new_intersection(2))<fabs(intersection(2)) && OccludedPlaneId !=-1)
            {
                intersection=new_intersection;
                disparity_plane=disparity_planes_all[vel][OccludedPlaneId];
                h_plane=planes_all[vel][OccludedPlaneId];
            }*/

            // missing points
            if (intersection==Vector3f(0,0,0))
            {
                if(DISCARD_NO_HIT)
                {
                    laser_in_camera_single[p] = intersection;
                    laser_points_single[p]=intersection;
                }
                else
                {
                    Vector3f new_intersection(0,0,0);
                    int dist;
                    int OccludedPlaneId=ProcessOccludedPoint(laser_in_camera_single[p],ray_ori,M_Proj,segments_all[vel],planes_all[vel],m,n,intersection,bb_all[vel],new_intersection,dist);
                    intersection=new_intersection;
                }
            }
            // faraway points
            if((intersection(2)>=max_point_dist && soft_intersection(2)<intersection(2) && soft_intersection(2)>0) || intersection(2)<0)
            {
                intersection=soft_intersection;
                disparity_plane=soft_disparity_plane;
                h_plane=soft_h_plane;
            }

            intersections_single[p]=intersection;
            disparity_plane_single[p]=disparity_plane;
            planes_single[p]=h_plane;

        }
        // end of loop for laser points
        double threepixelaccuracy, threepixelaccuracy2;
        ComparePoints(laser_in_camera_single,intersections_single,disparity_plane_single, laser_points_single, threepixelaccuracy, threepixelaccuracy2, M_v_c);
        threepixels.push_back(threepixelaccuracy2);

        string pt_name=point_name[vel];
        result_file<<pt_name<<" "<<threepixelaccuracy2<<endl;
    } // end of loop for different files
    double totalthree=0;
    for(int i=0;i<threepixels.size();i++)
    {
        totalthree+=threepixels[i];
    }
    totalthree/=threepixels.size();
    string threePixelName=res_path+"/tpe.txt";
    ofstream myFile2(threePixelName.c_str());
    myFile2<<totalthree<<endl;

    myFile2.close();
    result_file.close();
    #ifdef OPTIMIZE
    myfile<<delta_x<<" "<<delta_y<<" "<<delta_z<<" "<<delta_base<<" "<<totalthree<<endl;

   // }
    }
    }
    }
    myfile.close();
    #endif
    return 0;

}




