#ifndef CLOSESTSEGMENT_H_INCLUDED
#define CLOSESTSEGMENT_H_INCLUDED

#include"stdafx.h"

inline int findNearestSegment_approx(Vector2i &uv, std::vector<VectorXf> &bb, int segId) {
    if(bb.size()<segId+1)
    {
        return INT_MAX;
    }
    int u = uv(0), v = uv(1);
    float umin = bb[segId](0), umax = bb[segId](1);
    float vmin = bb[segId](2), vmax = bb[segId](3);
    // Case 1: fall into the bounding box
    if (u >= umin && u <= umax && v >= vmin && v <= vmax)
        return 0;
    // Case 2: distance to the bounding box
    // Divide the space into 8 regions as shown below:
    //   1 |    2    |  3
    //  ---|---------|-----    vmax
    //   4 | segment |  5
    //  ---|---------|-----    vmin
    //   6 |    7    |  8
    //    umin     umax

    if (u < umin && v > vmax) // in region 1
        return std::sqrt((u - umin)*(u - umin)+(v - vmax)*(v - vmax));
    if (u > umax && v > vmax) // in region 3
        return std::sqrt((u - umax)*(u - umax)+(v - vmax)*(v - vmax));
    if (v > vmax) // in region 2
        return v - vmax;
    if (u < umin && v < vmin) // in region 6
        return std::sqrt((u - umin)*(u - umin)+(v - vmin)*(v - vmin));
    if (u < umin) // in region 4
        return umin - u;
    if (u > umax && v < vmin) // in region 8
        return std::sqrt((u - umax)*(u - umax)+(v - vmin)*(v - vmin));
    if (v < vmin) // in region 7
        return vmin - v;
    if (u > umax) // in region 5
        return u - umax;

    return INT_MAX;
}

inline std::vector<VectorXf> computeBoundingBox(MatrixXf &seg) {

    int maxR = 0;
    for (unsigned int u = 0; u < seg.rows(); u++)
        for (unsigned int v = 0; v < seg.cols(); v++)
            maxR = maxR > seg(u, v) ? maxR : seg(u, v);
    std::cout << "maxR: " << maxR << std::endl;
    std::vector<VectorXf> bb(maxR + 1);
    int m = seg.rows();
    int n = seg.cols();
    for (int i = 0; i < maxR + 1; i++) {
        VectorXf newbb(4);
        newbb(0) = m;
        newbb(1) = 0;
        newbb(2) = n;
        newbb(3) = 0;
        bb[i] = newbb;
    }
    for (unsigned int u = 0; u < seg.rows(); u++) {
        for (unsigned int v = 0; v < seg.cols(); v++) {
            int segId = seg(u, v);
            bb[segId](0) = bb[segId](0) <= u ? bb[segId](0) : u;
            bb[segId](1) = bb[segId](1) >= u ? bb[segId](1) : u;
            bb[segId](2) = bb[segId](2) <= v ? bb[segId](2) : v;
            bb[segId](3) = bb[segId](3) >= v ? bb[segId](3) : v;
        }
    }
    return bb;
}



#endif // CLOSESTSEGMENT_H_INCLUDED
