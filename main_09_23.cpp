//#include <boost/numeric/ublas/vector.hpp>
//#include <boost/numeric/ublas/matrix.hpp>
#include <eigen3/Eigen/Dense>

#include <omp.h>
#include "dirent.h"
#include "string.h"
#include <stdlib.h> // atoi
#include <float.h>
#include <limits.h>
#include <queue>
#include <fstream>
#include <iostream>
#include <revlib.h>

#define EPS 1e-6


using namespace Eigen;
using namespace std;

double baseline_ratio;
double fx,fy,ox,oy;
double baseline;

std::vector<VectorXf> computeBoundingBox(MatrixXf &seg);
int findNearestSegment_approx(Vector2i &uv, std::vector<VectorXf> &bb, int segId);
std::vector<VectorXf> parseVelodynefile(const char *Velodynefile);
std::vector<VectorXf> parsePlanefile(const char *planefile,double ratio);
MatrixXf loadSegfile(const char *segfile);

std::vector<VectorXf > parseDisparityPlane(const char* planefile)
{
    std::vector<VectorXf > planes;
    std::ifstream input(planefile);
     if (!input.is_open()) {
        std::cout << "Open file error: " << planefile << std::endl;
        return planes;
    }
    string line;
    float tmp;
    char c;
    while(getline(input,line))
    {
        if(line[0]=='c')
            continue;
        istringstream is(line);
        VectorXf plane(5);
        is >> tmp;
        is >> plane(0);
        is >> plane(1);
        is >> plane(2);
        is >> plane(3);
        is >> tmp;
        is >> tmp;
        is >> tmp;
        is >> tmp;
		is >> c;
		if (c == 'Y')
			plane(4)=1;
		else
			plane(4)=0;
        planes.push_back(plane);

    }
    return planes;
}

void ComparePoints(std::vector<Vector3f > laser_in_camera, std::vector<Vector3f > intersections, std::vector<VectorXf > disparity_planes,  double& threepixelaccuracy)
{
    int num=laser_in_camera.size();
    int fiveperc=0,tenperc=0,totalpts=0,threepixel=0;
    cout<<laser_in_camera.size()<<" "<<intersections.size()<<endl;
    for(int i=0;i<num;i++)
    {
        if(!(laser_in_camera[i][0]==0 && laser_in_camera[i][1]==0 && laser_in_camera[i][2]==0))
        {
            totalpts++;
            //cout<<"total:"<<totalpts<<endl;
            double dist=(laser_in_camera[i][0]-intersections[i][0])*(laser_in_camera[i][0]-intersections[i][0])
            +(laser_in_camera[i][1]-intersections[i][1])*(laser_in_camera[i][1]-intersections[i][1])
            +(laser_in_camera[i][2]-intersections[i][2])*(laser_in_camera[i][2]-intersections[i][2]);
            dist=sqrt(dist);
            double length=laser_in_camera[i][0]*laser_in_camera[i][0]
            +laser_in_camera[i][1]*laser_in_camera[i][1]
            +(laser_in_camera[i][2])*(laser_in_camera[i][2]);
            length=sqrt(length);
           // cout<<length<<endl;
            double ratio=dist/length;
            if(ratio<0.05)
                fiveperc++;
            if(ratio<0.1)
                tenperc++;

            double X=laser_in_camera[i][0], Y=laser_in_camera[i][1], Z=laser_in_camera[i][2];
            double M1=disparity_planes[i][0], M2=disparity_planes[i][1], M3=disparity_planes[i][3];
           // cout<<M1<<" "<<M2<<" "<<M3<<endl;
           // cout<<X<<" "<<Y<<" "<<Z<<endl;
            double d_predict=fx*baseline/(baseline_ratio*Z);
            double e_predict=M1*(fx*X/Z+ox)+M2*(fy*Y/Z+oy)+M3;
            if(fabs(d_predict-e_predict)<=3)
            {
                threepixel++;
            }
        }
    }

    std::cout<<"total points: "<<totalpts<<std::endl;
    threepixelaccuracy=(double)threepixel/totalpts;
    std::cout<<"error: "<<" "<<(double)fiveperc/(double)totalpts<<" "<<(double)tenperc/(double)totalpts<<" "<<threepixelaccuracy<<std::endl;
}

inline VectorXf Initialize(int n, int* a=NULL)
{
    VectorXf v(n);
    for(int i=0;i<n;i++)
    {
        if(a!=NULL)
            v[i]=a[i];
        else
            v[i]=0;
    }
    return v;
}


int main(int argc, char *argv[]) {

    if (argc != 6) {
        std::cout << "Usage: ./castRay Velo_dir segfile_dir planefile_dir CalibMatrixFile BaselineRatioFile" << std::endl;
        return -1;
    }
    const char *path = argv[1];
    const char *segpath = argv[2];
    const char *planepath = argv[3];
	const char *calibfilename = argv[4];
	const char *baselinefilename=argv[5];

	omp_set_num_threads(omp_get_max_threads());

	Matrix4f M_v_c;
	M_v_c << 0, -1 ,0, 0,
			 0,0,-1,0,
		       1,0,0,0,
				0,0,0,1;

    std::ifstream calibfile(calibfilename);
    if (!calibfile.is_open()) {
        std::cout << "Open file error: " << calibfilename << std::endl;
		return -1;
    } else {
		int i=0, j=0;
    	while (!calibfile.eof() && i<4) {
			calibfile >> M_v_c(i,j);
			if (++j>3) {
				j=0;
				i++;
			}
    	}
	}

	ifstream baselinefile(baselinefilename);
	if(!baselinefile.is_open())
	{
        std::cout << "Open file error: " << baselinefilename << std::endl;
		return -1;
	}
	else
	{
        baselinefile>>baseline_ratio;
	}
	baselinefile.close();

    Matrix3f M_Proj;
    M_Proj(0, 0) = 1019.305;
    M_Proj(0, 1) = 0.0;
    M_Proj(0, 2) = 789.380;
    M_Proj(1, 0) = 0.0;
    M_Proj(1, 1) = 1019.305;
    M_Proj(1, 2) = 612.212;
    M_Proj(2, 0) = 0.0;
    M_Proj(2, 1) = 0.0;
    M_Proj(2, 2) = 1.0;

    fx=M_Proj(0,0);
    fy=M_Proj(1,1);
    ox=M_Proj(0,2);
    oy=M_Proj(1,2);
    baseline=0.558253;

    /*matrix<float> R_rect(3,3);
    R_rect(0,0) = 9.999239e-01;
    R_rect(0,1) = 9.837760e-03;
    R_rect(0,2) = -7.445048e-03;
    R_rect(1,0) = -9.869795e-03;
    R_rect(1,1) = 9.999421e-01;
    R_rect(1,2) = -4.278459e-03;
    R_rect(2,0) = 7.402527e-03;
    R_rect(2,1) = 4.351614e-03;
    R_rect(2,2) = 9.999631e-01;*/

    VectorXf origin(4);
    int thres = 10;

    std::cout << "Opening path: " << path << std::endl;
    DIR* VelodyneDir = opendir(path);
    if (VelodyneDir) {
        struct dirent* vFile;
        int VelInd = 0;
		std::vector<std::vector<VectorXf> > laser_points_all;
		std::vector<MatrixXf> segments_all;
		std::vector<std::vector<VectorXf> > planes_all;
		std::vector<std::vector<VectorXf> > bb_all;
        std::vector<std::vector<VectorXf> > disparity_planes_all;
		int m=0, n=0;
        while ((vFile = readdir(VelodyneDir)) != NULL) {
            if (strcmp(vFile->d_name, ".") == 0) continue;
            if (strcmp(vFile->d_name, "..") == 0) continue;

            //if (strcmp(vFile->d_name, "000200.txt") != 0) continue;
            VelInd++;
            std::cout << "Opening laser file: " << vFile->d_name << std::endl;
            char *fullpath = (char*) malloc(strlen(path) + strlen(vFile->d_name) + 2);
            fullpath[0] = '\0';
            strcat(fullpath, path);
            strcat(fullpath, vFile->d_name);
            std::vector<VectorXf> laser_point = parseVelodynefile(fullpath);
			laser_points_all.push_back(laser_point);
            std::cout << "parse velodyne file finished." << std::endl;
			free(fullpath);

			fullpath = (char*) malloc(strlen(segpath) + strlen(vFile->d_name) + 6);
			fullpath[0] = '\0';
            strcat(fullpath, segpath);
            strcat(fullpath, vFile->d_name);
			fullpath[strlen(fullpath)-10] = '\0';
			strcat(fullpath, "_seg.png");
            MatrixXf segment = loadSegfile(fullpath);
			segments_all.push_back(segment);
            std::cout << "load seg file finished." << std::endl;
			free(fullpath);

			fullpath = (char*) malloc(strlen(planepath) + strlen(vFile->d_name) + 13);
			fullpath[0] = '\0';
            strcat(fullpath, planepath);
            strcat(fullpath, vFile->d_name);
			fullpath[strlen(fullpath)-10] = '\0';
			strcat(fullpath, "_seg_planes.txt");
            std::vector<VectorXf> planes = parsePlanefile(fullpath,baseline_ratio);
            std::vector<VectorXf> disparityPlanes=parseDisparityPlane(fullpath);
			planes_all.push_back(planes);
			disparity_planes_all.push_back(disparityPlanes);
            std::cout << "parse plane file finished." << std::endl;
			free(fullpath);

            if (m==0 && n==0){
				m = segment.rows();
            	n = segment.cols();
			}
			else
				if (m!=segment.rows() || n!=segment.cols()) {
					std::cout << "error: image size does not match!" << std::endl;
				}
            std::vector<VectorXf> bb = computeBoundingBox(segment);
			bb_all.push_back(bb);
		} // end of scanning velodyne folder
        closedir(VelodyneDir);

		unsigned int total_points = 0;
		for (unsigned i=0; i<laser_points_all.size(); i++)
			total_points += laser_points_all[i].size();

        Matrix4f M_v_c_old=M_v_c;
        double baseline_ratio_old=baseline_ratio;
        ofstream accuracy_file("accuracy.txt");
        //for(double ii=-0.2;ii<=0.2;ii+=0.01)
        //{
            //for(double jj=-0.1;jj<=0.1;jj+=0.01)
            //{
                //for(double kk=-0.2;kk<=0.2;kk+=0.01)
                //{
                    //for(double tt=-0.1;tt<=0.1;tt+=0.01)
                    //{
                        //M_v_c=M_v_c_old;
                       // M_v_c(0,3)+=ii;
                        //M_v_c(1,3)+=jj;
                       // M_v_c(2,3)+=kk;
                        //baseline_ratio=baseline_ratio_old;
                        //baseline_ratio+=tt;

        std::vector<Vector3f> intersections;
        std::vector<Vector3f> laser_in_camera;
        std::vector<std::pair<int,int> > iplane; // saving the computed intersection plane id for each laser beam

            origin(0) = 0;
            origin(1) = 0;
            origin(2) = 0;
            origin(3) = 1;
            origin = M_v_c * origin;
            Vector3f ray_ori;
            ray_ori(0) = origin(0);
            ray_ori(1) = origin(1);
            ray_ori(2) = origin(2);

            std::cout << "ray origin: " << ray_ori(0) << " " << ray_ori(1) << " " << ray_ori(2) << std::endl;
            vector<double> threepixels;

#pragma omp parallel for
			for (unsigned vel=0; vel<laser_points_all.size(); vel++) {
                if(vel%4!=0)
                    continue;
                int ptnum=laser_points_all[vel].size();
                vector<Vector3f> laser_in_camera_single(ptnum), intersections_single(ptnum);
                vector<VectorXf> disparity_plane_single(ptnum);

                for (int p = 0; p < ptnum; p++) {
                    if (p % 10000 == 0) std::cout << "." << std::flush;
                    VectorXf disparity_plane=Initialize(4);
                    Vector3f intersection(0,0,0);
					int inter_plane = -1;
                    VectorXf point = laser_points_all[vel][p];
                    if (point(0) == 0 && point(1) == 0 && point(2) == 0) {
                        laser_in_camera_single[p]=intersection;
                        intersections_single[p]=intersection;
                        disparity_plane_single[p]=Initialize(4);// discard the zero laser point
                        continue;
                    }
                    VectorXf X(4);
                    X(0) = point(0);
                    X(1) = point(1);
                    X(2) = point(2);
                    X(3) = 1;
                    X = M_v_c * X;
                    Vector3f X_trans;
                    X_trans(0)=X(0);
                    X_trans(1)=X(1);
                    X_trans(2)=X(2);

                    Vector3f UV = M_Proj * X_trans;
                    float u = UV(0) / UV(2), v = UV(1) / UV(2);
                    //if (u < 0 || round(u) >= m || v < 0 || round(v) >= n || X(2) <= 0)
                    if(u-1<=150 || u>=m-50 || v-1<=0 || v>=n-1 || X(2)<=0)
                    {
                        laser_in_camera_single[p]=intersection;
                        intersections_single[p]=intersection;
                        disparity_plane_single[p]=Initialize(4);
                        continue;
                    }

                    // Discard the laser point if it is projected outside the camera frame
                    Vector3f lasercam = X_trans;
                    laser_in_camera_single[p] = X_trans;
                    Vector3f ray_unit = X_trans - ray_ori;
                    ray_unit=ray_unit/ray_unit.norm();


                    float mindist = FLT_MAX,mindist2=FLT_MAX;
                    for (int i = 0; i < planes_all[vel].size(); i++) {
                        double u, v, s;

                        /* Finding intersection of ray and a plane */
                        VectorXf plane = planes_all[vel][i];
                        plane[0]*=baseline_ratio;
                        plane[1]*=baseline_ratio;
                        plane[2]*=baseline_ratio;

                        if (plane[4] == 0) continue; // The plane is marked as in-valid
                        Vector3f plane_coef;
                        plane_coef(0) = plane(0);
                        plane_coef(1) = plane(1);
                        plane_coef(2) = plane(2);
                        double coef = plane_coef.transpose() * ray_unit;
                          VectorXf X_in;
                        if (std::abs(coef) > EPS) {
                            s = (plane(3) + plane_coef.transpose() * ray_ori) / -coef;
                            X_in = ray_ori + s * ray_unit;
                            Vector3f U_in = M_Proj * X_in;
                            u = U_in(0) / U_in(2);
                            v = U_in(1) / U_in(2);
                            //}
                        } else {
                            u = -1;
                            v = -1;
                            s = -1;
                        }
                        if (s < 0)
                            continue;
            //harry code: crop the left and right boundary
                       /* if (u - 1 <= 150 || u >= m-50)
                        {
                          //  laser_in_camera_single[p]=intersection;
                            continue;
                        }
                        if (v - 1 <= 0 || v >= n-1)
                        {
                        //    laser_in_camera_single[p]=intersection;
                            continue;
                        }*/
                         if (u - 1 <= 0 || u >= m-1)
                            continue;
                        if (v - 1 <= 0 || v >= n-1)
                            continue;

                        double ss=(X(0)-X_in(0))*(X(0)-X_in(0))+(X(1)-X_in(1))*(X(1)-X_in(1))+(X(2)-X_in(2))*(X(2)-X_in(2));

                        if (segments_all[vel](round(u), round(v)) == i) // Case 1: Intersection point fall into the same segment region
                        {
                            if (ss < mindist2 && s > 0)
                            //if(s<mindist && s>0)
                            { // finding the intersection with the minimal distance
                                mindist = s;
                                mindist2=ss;
                                intersection = ray_ori + s * ray_unit;
                                disparity_plane=disparity_planes_all[vel][i];
                            }
                        } else {
                            Vector2i uv;
                            uv(0) = round(u);
                            uv(1) = round(v);
                            //int dis = findNearestSegment(uv, segment, i, distmap, thres); // The BFS method - computationally expensive
                            int dis = findNearestSegment_approx(uv, bb_all[vel], i); // An Bounding-box approximate method
                            if (dis > thres)
                                continue;
                            else // Case 2: Intersection point fall within a certain distance to the segment region
                            {
                               //  if (mindist < FLT_MAX && ss < mindist2 && s > 0) {
                               if(ss<mindist2 && s>0){
                               // if(s<mindist && s>0){
                                    mindist = s;
                                    mindist2=ss;
                                    intersection = ray_ori + s * ray_unit;
                                    disparity_plane=disparity_planes_all[vel][i];
                                }
                            }
                        }

                    } // end of loop for planes
                    intersections_single[p]=intersection;
                    if (intersection(0) == 0 && intersection(1) == 0 && intersection(2) == 0){
                        laser_in_camera_single[p] = intersection;
                    }
                    disparity_plane_single[p]=disparity_plane;

					#pragma omp critical
					{
						laser_in_camera.push_back(lasercam);
                    	intersections.push_back(intersection);
						iplane.push_back(std::pair<int,int>(vel,inter_plane));

					}

                } // end of loop for laser points
                double threepixelaccuracy;
                ComparePoints(laser_in_camera_single,intersections_single,disparity_plane_single, threepixelaccuracy);
                threepixels.push_back(threepixelaccuracy);
			} // end of loop for different files
			double totalthree=0;
            for(int i=0;i<threepixels.size();i++)
            {
                totalthree+=threepixels[i];
            }
            totalthree/=threepixels.size();
            cout<<"average three pixel: "<<totalthree<<endl;
          //  accuracy_file<<M_v_c<<endl;
          //  accuracy_file<<baseline_ratio<<endl;
         //   accuracy_file<<"three pixel: "<<totalthree<<endl<<endl;

            //}
            //}
            //}
            //}
            /* Output the ray-tracing result */

    accuracy_file.close();

    } else
        std::cout << "Error open the Velodyne folder!" << std::endl;

    return 0;

}


int findNearestSegment_approx(Vector2i &uv, std::vector<VectorXf> &bb, int segId) {

    int u = uv(0), v = uv(1);
    float umin = bb[segId](0), umax = bb[segId](1);
    float vmin = bb[segId](2), vmax = bb[segId](3);
    // Case 1: fall into the bounding box
    if (u >= umin && u <= umax && v >= vmin && v <= vmax)
        return 0;
    // Case 2: distance to the bounding box
    // Divide the space into 8 regions as shown below:
    //   1 |    2    |  3
    //  ---|---------|-----    vmax
    //   4 | segment |  5
    //  ---|---------|-----    vmin
    //   6 |    7    |  8
    //    umin     umax

    if (u < umin && v > vmax) // in region 1
        return std::sqrt((u - umin)*(u - umin)+(v - vmax)*(v - vmax));
    if (u > umax && v > vmax) // in region 3
        return std::sqrt((u - umax)*(u - umax)+(v - vmax)*(v - vmax));
    if (v > vmax) // in region 2
        return v - vmax;
    if (u < umin && v < vmin) // in region 6
        return std::sqrt((u - umin)*(u - umin)+(v - vmin)*(v - vmin));
    if (u < umin) // in region 4
        return umin - u;
    if (u > umax && v < vmin) // in region 8
        return std::sqrt((u - umax)*(u - umax)+(v - vmin)*(v - vmin));
    if (v < vmin) // in region 7
        return vmin - v;
    if (u > umax) // in region 5
        return u - umax;

    return INT_MAX;
}

std::vector<VectorXf> computeBoundingBox(MatrixXf &seg) {

    int maxR = 0;
    for (unsigned int u = 0; u < seg.rows(); u++)
        for (unsigned int v = 0; v < seg.cols(); v++)
            maxR = maxR > seg(u, v) ? maxR : seg(u, v);
    std::cout << "maxR: " << maxR << std::endl;
    std::vector<VectorXf> bb(maxR + 1);
    int m = seg.rows();
    int n = seg.cols();
    for (int i = 0; i < maxR + 1; i++) {
        VectorXf newbb(4);
        newbb(0) = m;
        newbb(1) = 0;
        newbb(2) = n;
        newbb(3) = 0;
        bb[i] = newbb;
    }
    for (unsigned int u = 0; u < seg.rows(); u++) {
        for (unsigned int v = 0; v < seg.cols(); v++) {
            int segId = seg(u, v);
            bb[segId](0) = bb[segId](0) <= u ? bb[segId](0) : u;
            bb[segId](1) = bb[segId](1) >= u ? bb[segId](1) : u;
            bb[segId](2) = bb[segId](2) <= v ? bb[segId](2) : v;
            bb[segId](3) = bb[segId](3) >= v ? bb[segId](3) : v;
        }
    }

    return bb;
}



MatrixXf loadSegfile(const char *segfile) {

    rev::Image<unsigned short> segImage;
    segImage = rev::read16bitImageFile(segfile);

    MatrixXf segment(segImage.width(), segImage.height());
    for (int u = 0; u < segImage.width(); u++)
        for (int v = 0; v < segImage.height(); v++)
            segment(u, v) = segImage(u, v);

    return segment;

}

std::vector<VectorXf> parsePlanefile(const char *planefile,double opt) {

     std::vector<VectorXf> planes;
    std::ifstream input(planefile);
    if (!input.is_open()) {
        std::cout << "Open file error: " << planefile << std::endl;
        return planes;
    }
    string line;
    float tmp;
	char c;
    while(getline(input,line))
    {
        if(line[0]=='c')
            continue;
        istringstream is(line);
        VectorXf plane(5);
        is >> tmp;
        is >> tmp;
        is >> tmp;
        is >> tmp;
        is >> tmp;
        is >> plane(0);
        is >> plane(1);
        is >> plane(2);
        is >> plane(3);
        plane(0)=plane(0);
        plane(1)=plane(1);
        plane(2)=plane(2);
		is >> c;
		if (c == 'Y')
			plane(4)=1;
		else
			plane(4)=0;
        planes.push_back(plane);
    }
    return planes;
}

std::vector<VectorXf> parseVelodynefile(const char *Velodynefile) {

    std::vector<VectorXf> laser_points;
    std::ifstream input(Velodynefile);
    if (!input.is_open()) {
        std::cout << "Open file error: " << Velodynefile << std::endl;
        return laser_points;
    }

    float tmp;
    while (!input.eof()) {
        VectorXf point(6);
        input >> tmp;
        point(0) = tmp / 100.0;
        input >> tmp;
        point(1) = tmp / 100.0;
        input >> tmp;
        point(2) = tmp / 100.0;
        input >> tmp;
        point(3) = tmp;
        input >> tmp;
        point(4) = tmp;
        input >> tmp;
        point(5) = tmp;
        laser_points.push_back(point);
    }
    return laser_points;
}
