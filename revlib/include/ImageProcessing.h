#ifndef REVLIB_IMAGE_PROCESSING_H
#define REVLIB_IMAGE_PROCESSING_H

#include <vector>
#include "Image.h"

namespace rev {
    
void convertImageUCharToFloat(const Image<unsigned char>& inputUCharImage, Image<float>& outputFloatImage);
void convertImageFloatToUChar(const Image<float>& inputFloatImage, Image<unsigned char>& outputUCharImage);
    
void convertImageRGBToLab(const Image<unsigned char>& rgbImage, Image<float>& labImage);
    
void gaussianSmooth(const Image<float>& sourceImage, const double sigma, Image<float>& gaussianImage);

}

#endif
